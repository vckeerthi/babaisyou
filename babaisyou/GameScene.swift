//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    var baba:SKSpriteNode!
    let  babaspeed:CGFloat = 20
    var wallblock : SKSpriteNode!
    let wallblockspeed : CGFloat = 20
    var isblock : SKSpriteNode!
    var winblock : SKSpriteNode!
    let winblockspeed:  CGFloat = 20
    var stopblock: SKSpriteNode!
    let stopblockspeed : CGFloat  = 20
    var flagblock: SKSpriteNode!
    let flagblockspeed: CGFloat = 20
    var wall : SKSpriteNode!
    var flag: SKSpriteNode!
    
    
    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        self.baba  = self.childNode(withName: "baba")as! SKSpriteNode
        self.wallblock = self.childNode(withName: "wallblock") as! SKSpriteNode
        self.isblock = self.childNode(withName: "isblock") as! SKSpriteNode
        self.winblock = self.childNode(withName: "winblock") as! SKSpriteNode
        self.stopblock = self.childNode(withName: "stopblock") as! SKSpriteNode
        self.wall = self.childNode(withName: "wall") as! SKSpriteNode
        self.flagblock = self.childNode(withName: "flagblock") as! SKSpriteNode
        
        self.baba?.physicsBody?.categoryBitMask = 1
        self.isblock?.physicsBody?.categoryBitMask = 2
        self.flag?.physicsBody?.categoryBitMask = 4
        self.wall?.physicsBody?.categoryBitMask = 8
        self.wallblock?.physicsBody?.categoryBitMask  = 16
        self.flagblock?.physicsBody?.categoryBitMask = 16
        self.winblock?.physicsBody?.categoryBitMask = 16
        self.stopblock?.physicsBody?.categoryBitMask = 16
        
        self.wall?.physicsBody?.collisionBitMask = 27
        self.baba?.physicsBody?.collisionBitMask = 21
        self.flag?.physicsBody?.collisionBitMask  = 5
        self.isblock?.physicsBody?.collisionBitMask = 3
        self.winblock?.physicsBody?.collisionBitMask = 25
        self.flagblock?.physicsBody?.collisionBitMask = 25
        self.winblock?.physicsBody?.collisionBitMask  = 25
        self.stopblock?.physicsBody?.collisionBitMask = 25
        
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        //sdjfhjk
        if (nodeA == nil || nodeB == nil) {
            return
            
        }
        
        if (nodeA!.name == "baba" && nodeB!.name == "wallblock") {
            
         
            print("collision occured")
            
        }
        
        print("Something collided!")
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let mouseTouch = touches.first
        if (mouseTouch == nil) {
            return
        }
        let location = mouseTouch!.location(in: self)
        // WHAT NODE DID THE PLAYER TOUCH
        // ----------------------------------------------
        let nodeTouched = atPoint(location).name
        //print("Player touched: \(nodeTouched)")
        // GAME LOGIC: Move player based on touch
        if (nodeTouched == "upbutton") {
            // move up
            self.baba.position.y = self.baba.position.y + babaspeed
        }
        else if (nodeTouched == "downbutton") {
            // move down
            self.baba.position.y = self.baba.position.y - babaspeed
        }
        else if (nodeTouched == "leftbutton") {
            // move left
            self.baba.position.x = self.baba.position.x - babaspeed
        }
        else if (nodeTouched == "rightbutton") {
            // move right
            self.baba.position.x = self.baba.position.x + babaspeed
        }
      
        
        

    }
    
   
}

